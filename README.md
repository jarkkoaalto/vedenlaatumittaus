##README

#  Ympäristömittaus  #

# Johdanto #

### Toimeksianto ###

Toteutamme koko TE-keskuksen IoT-täydennyskoulutus opetusryhmän voimin mittausprojekti, jossa sovelletaan opittuja asioita. Työn kuluessa opitaan, mitä IoT-mittauslaitteisto on ja siihen liittyvän web-palvelun ylläpito käytännössä vaatii. Projekti on, henkilökohtaisten harjoitusprojektien lisäksi, hyvä “referenssi” kurssilla opituista asioista.

Toteutetaan vedenlaadun mittauspiste lähiseudun järven tai joen vedenlaadun mittaamiseksi. Laitteisto toimii autonomisesti, mutta sitä tulee silloin tällöin huoltaa ja puhdistaa. Laitteisto toteutetaan 1 - 1,5 m mittaiseen PVC -muoviputkeen ja se asennetaan työntämällä anturiin liittyvä asennusvarsi pohjaan tai vaihtoehtoisesti anturi kiinnitetään putkikiinnikkeellä laituriin tms.

* Laitteistossa on johtokyky- ja sameusanturi, joissa on taajuuslähtö. 

* Antureiden taajuutta luetaan Arduinolla (nano). Taajuuslukemat viedään sarjadatana MQTT-gateway:na toimivaan Arduinoon (+GSM Shield), joka välittää datan brokerille (CloudMQTT). 

* Laitteistossa on ajastettu kytkin, joka on toteutettu AT Tiny85-prosessorista ja FET-kytkimestä. Pienellä virralla toimiva kytkin on jatkuvasti virroitettu, ja se kytkee paljon virtaa kuluttavat GW:n ja anturit kerran 60 minuutissa päälle. GW kuuntelee lähetyksen aikana brokeria, ja kun lähetys on onnistunut, antureiden ja GW:n virta katkaistaan. Laitteistossa on monitorointi myös käyttöakkujen napajännitteelle.

* Ryhmän tulee suunnitella ja toteuttaa web-palvelu, josta näkyvät mittaukset ja akkujen lataustila. Käytetään mielellään muita palveluita kuin ThingSpeakia, Mongon ja R:n hyödyntäminen on suositeltavaa.

* Ke 8.3. kasataan laitteiston “leipälautaversio”, jonka avulla a) tutustutaan laitteiston toimintaan ja b) lähdetään toteuttamaan web-palvelua. Samalla mietitään työnjako ja organisointi. Ulos asennettava laitteisto valmistuu viikoksi 12.


Vesimittausprojektin raaka laitteistosuunnittelu on esitelty (ks. Kuvio alla)
![vesiprojekti.png](https://bitbucket.org/repo/p44AKBr/images/19280749-vesiprojekti.png)

### Mittalaitteiston kalibrointi ###

Mittalaitteiston sameuden kalibrointi toteutettiin maidolla, jota laimennettiin vedellä peräkkäisillä mittauskerroilla. Johtavuuden kalibrointi toteutettiin lisäämällä ruokalusikallinen merisuolaa veteen, jota sitten laimennettiin peräkkäisillä mittauskerroilla. Mittaustulokset ja laimennuksien % -osuudet on esitelty (ks. Kuvio alla)

![mittaukset.png](https://bitbucket.org/repo/p44AKBr/images/2778314391-mittaukset.png)

### Toimiva sovellus ###

Toimiva veden laadun mittalaitteistosta saatu data on julkisesti saatavilla Thingspeak.com www-sivustolla (ks. Linkki [Vesimittaus](https://thingspeak.com/channels/252090))



Laitteiston mittapään sameus- ja johtavuusmittausyksikkö (ks. Kuvio alla)

![mittapaa.png](https://bitbucket.org/repo/p44AKBr/images/3340651122-mittapaa.png)

Laitteiston hallintayksikkö (ks. Kuvio alla)

![latteistokotelo.jpg](https://bitbucket.org/repo/p44AKBr/images/3948480103-latteistokotelo.jpg)

Mittauslaitteisto toiminnassa (ks. Kuvio alla)

![mittalaitteisto.png](https://bitbucket.org/repo/p44AKBr/images/3915868926-mittalaitteisto.png)




## Laitteistosuunnittelu dokumentit ##

Laitteistossa on ajastettu kytkin, joka on toteutettu AT Tiny85-prosessorista ja FET-kytkimestä. Kytkin kytkee paljon virtaa kuluttavat GW:n ja anturit kerran 60 minuutissa päälle. Antureiden taajuutta luetaa Arduinolla (nano). Taajuuslukemat viedään (+GSM Shield) varustettuun Arduinoon. Laitteistosuunnitteludokumentti on esitelty (ks. Kuvio alla). 
 
![Vesiprojekti_bb.png](https://bitbucket.org/repo/p44AKBr/images/2083001148-Vesiprojekti_bb.png)


## Ohjelmistosuunnittelu dokumentit ##

### Releen ohjaus ###
Laitteistossa on ajastettu kytkin, joka on toteutettu AT Tiny85-prosessorista ja FET-kytkimestä. Pienellä virralla toimiva kytkin on jatkuvasti virroitettu, ja se kytkee paljon virtaa kuluttavat GW:n ja anturit kerran 60 minuutissa päälle. GW kuuntelee lähetyksen aikana brokeria, ja kun lähetys on onnistunut, antureiden ja GW:n virta katkaistaan. Laitteistossa on monitorointi myös käyttöakkujen napajännitteelle. (ks. koodi alla).

```
#!c

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

int main (void)
{
	//DDRB &= ~(1 << PB4); 	
	DDRB |= (1 << PB3); 	
	PORTB &= ~(1 << PB3); 	

	_delay_ms(50000);

	while (1) {
		PORTB |= (1 <<PB3);		// reley off
		for (int i=0; i<60; i++){ _delay_ms(60000); } 
		PORTB &= ~(1 << PB3); 	       // reley on
		 _delay_ms(50000);
	}
	
	return 1;
}

```

### Johtavuus ja sameusmittaus ###
Laitteistossa on johtokyky- ja sameusanturi, joissa on taajuuslähtö. 
- Antureiden taajuutta luetaan Arduinolla (nano). (ks. koodi alla)

```
#!c
void setup() {
  
  pinMode(11, INPUT);
  pinMode(12, INPUT);
  Serial.begin(9600);
}

void loop() {
  String send = "{[ Frequency:" + String(freq_count(11)) +", "
                "Conductivity:" + String(freq_count(12)) + "]}\n" ;
Serial.println(send);
// one minute delay, is it enough?
delay(60000);
}

long unsigned int freq_count(int pin) {  
  int val = 0;
  int val_old = 0;
  int meascount = 0;
  int diff = 0;
  long unsigned start = micros(); // start the timer
  
  while(meascount < 5) {
    val = digitalRead(pin);
    diff = val - val_old;
    
    if(diff==1) { //detect rising edge
      meascount +=1; //count the edges four times
      }
      
    val_old = val;
    }
    
    long unsigned int period = micros() - start; //calculate the time
    long unsigned return_val =  period/4;
    return return_val;
}
```
### MMQT GSM-shield ###
Taajuuslukemat viedään sarjadatana MQTT-gateway:na toimivaan Arduinoon (+GSM Shield), joka välittää datan brokerille (CloudMQTT) (ks. koodi alla).

```
#!c

#include <PubSubClient.h>
#include <GSM.h>

// PIN number if necessary
#define PINNUMBER ""

// APN information obrained from your network provider
#define GPRS_APN "data.dna.fi" // replace with your GPRS APN
#define GPRS_LOGIN "" // replace with your GPRS login
#define GPRS_PASSWORD "" // replace with your GPRS password

// initialize the library instances
GSMClient client;
GPRS gprs;
GSM gsmAccess;

//const char* server = "m13.cloudmqtt.com";
PubSubClient mqttClient(client);

void callback(char* topic, byte* payload, unsigned int length) {
  
  Serial.print("message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  if ((char)payload[0]!=NULL) {
    digitalWrite(10, HIGH); }
}

void setup()
{
  Serial.begin(9600);
  StartGSM(); 
  connect_broker();
  digitalWrite(10, LOW);
  pinMode(10, OUTPUT);
  //digitalWrite(10, HIGH);
  //delay(2000);
  
}
bool has_read = false;

void loop() {
//bool has_read = false;

String Incoming = "";

 if (Serial.available() > 0 && has_read==false) {
  //if (Serial.read() == '{') {
     //Incoming = '{' + Serial.readString(); 
     Incoming = Serial.readString();
     Incoming.trim();
     has_read=true;    
     digitalWrite(10, LOW);
    //}
  }

  
  if (!mqttClient.connected()) {
    connect_broker();
  }
  
  if (has_read==true) {
    publish_message(Incoming);
    mqttClient.loop();
    has_read=false;
  }

  if (millis()>40000) {
    digitalWrite(10, HIGH);
  }
}

void StartGSM() {

 Serial.println("starting GSM web client.");
 // connection state
 boolean notConnected = true;

 // Start GSM shield
 // pass the PIN of your SIM as a parameter of gsmAccess.begin()
  while(notConnected) {
    if((gsmAccess.begin(PINNUMBER)==GSM_READY) &
      (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD)==GPRS_READY))
      notConnected = false;
    else {
    Serial.println("not connected");
    delay(1000);
    }
  }
}

void connect_broker() {
  char server[] = "m13.cloudmqtt.com"; // the broker URL
  int port = 15840; // the port
  Serial.print("connecting to broker...");
  mqttClient.setServer(server, port);
  mqttClient.setCallback(callback);

  while(!mqttClient.connected()) {
    if(mqttClient.connect("ArduinoGSMid", "xxlaitaomaxx", "xxlaitaomaxx")) { //temporary client id from HiveMQ dashboard
      Serial.println(" connected");
      // connection succeeded
      mqttClient.subscribe("mittaukset/hello");
    } else {
    // connection failed
    Serial.print("broker connection failed, rc=");
    Serial.print(mqttClient.state());// will provide more information on failure
    Serial.println(" try again in 5 seconds");
    delay(5000);
    }
  }
}

void publish_message(String data_string_) {
    char data_string[100];
    data_string_.toCharArray(data_string, data_string_.length() + 1);
    Serial.println("publish_message");
    Serial.println(data_string);
  if(mqttClient.connect("ArduinoGSMid")) {
    //mqttClient.publish("mittaukset/hello", data_string);
    mqttClient.publish("mittaukset/hello", data_string);
    delay(3000);
  }
}

```

### Yhteys ja tiedonsiirto Mongodb tietokannasta  ###

Yhteyden muodostaminen ja tiedonsiirto MongoDB tapahtuu Nodejs hyväksikäyttäen. Yhteyden muodostus tapahtuu rivillä 2 URI-yhteyttä hyväksikäyttämällä, jossa localhost:portti, käyttäjänimi sekä salasana.

```
#!js

var mqtt  = require('mqtt');
var mqttClient  = mqtt.connect('<xxxxyouownxxxxx>',{
  username: '<xxxxyouownxxxxx>',
  password: '<xxxxyouownxxxxx>'
});

var MongoClient = require('mongodb').MongoClient;
 
var myCollection;
var db;
var pl;


mqttClient.on('message', function(topic, message) {
  console.log(message.toString('utf8'));
  pl = message.toString('utf8');

	createConnection(function(){
    		addDocument(function(){
		});
	});

});

mqttClient.on('connect', function(){
    console.log('Connected');
});

mqttClient.subscribe('mittaukset/#');
 
function addDocument(onAdded){
    myCollection.insert({Time: timeConverter(Date.now()), payload: pl}, function(err, result) {
        if(err)
            throw err;
 
        console.log("entry saved");
        onAdded();
    });
}
 
function createConnection(onCreate){
    MongoClient.connect('<xxxxyouownxxxxx>', function(err, db) {
        if(err)
            throw err;
        console.log("connected to the mongoDB !");
        myCollection = db.collection('test_collection');
 
        onCreate();
		db.close();
    });
}
 
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}

```
### Mittalaitteistosta saadun tiedon siirto Thinkspeakiin ###

```
#!c

# -*- coding: utf-8 -*-
import os
import paho.mqtt.client as mqtt
import urllib2
import time
import datetime
from time import sleep

TS_delay = 15

start_time = time.time()
cond = 0
turb = 0
UpdateTS = False

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("mittaukset/hello")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):

    myAPI = "T0T2BUES7KRFZN59"
    baseURL = 'https://api.thingspeak.com/update?api_key=%s' % myAPI

    global start_time
    global cond
    global turb

    UpdateTS = False
    
    print(msg.topic+" "+str(msg.payload))

    johtavuus = str(msg.payload).split(":")[1].split(".")[0].strip()
    sameus = str(msg.payload).split(":")[2].split(".")[0].strip()

    #print(sameus)
    #print(johtavuus)

    #sameus min = 53500
    #sameus max = 69700

    #johtokyky max = 150500
    #johtokyky min = 200
    
    
    sameusarvo = int(sameus)*10000/70000
    print(sameusarvo)

    if(sameusarvo < 0):
        sameusarvo = 0

    j_arvo = 150500*50/int(johtavuus)
    print(j_arvo)

    if(time.time() - start_time > TS_delay):
        start_time = time.time()
        UpdateTS = True

    if(UpdateTS==True):
        try:
            f = urllib2.urlopen(baseURL +
                           "&field1=%s&field2=%s&field3=%s&field4=%s" % (johtavuus, sameus, sameusarvo, j_arvo))
            print(f.read())
            f.close()

            start_time = time.time()

        except:
            print('exiting.')
            pass

#        updates +=1
    UpdateTS = False

#if(updates==90):
#    os.system('sudo shutdown -r now')
#    time.sleep(10)

time.sleep(20)
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

time.sleep(5)
client.username_pw_set("<xxxxxxxxxxxx>","<XXXXXXXXXX>")
client.connect("XX", XX, XX)

client.loop_forever()
```